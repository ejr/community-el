/* -*- mode: C; mode: folding; fill-column: 70; -*- */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include <assert.h>

#if !defined(__MTA__)
#include <libgen.h>
#endif
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>

#include "xmalloc.h"
#include "prng.h"

#if defined(__MTA__)
#define MTA(x) _Pragma(x)
#if defined(MTA_STREAMS)
#define MTASTREAMS() MTA(MTA_STREAMS)
#else
#define MTASTREAMS() MTA("mta use 100 streams")
#endif
#else
#define MTA(x)
#define MTASTREAMS()
#define readfe(P_) (*(P_))
#define writeef(P_,V_) (*(P_)=(V_))
#define writeff(P_,V_) (*(P_)=(V_))
#endif

#if defined(_OPENMP)
#include <omp.h>
#define OMP(x) _Pragma(x)
#else
#define OMP(x)
#endif

MTA("mta inline")
MTA("mta expect parallel")
static int64_t
int64_fetch_add (int64_t * p, int64_t incr)
{
#if defined(__MTA__)
  return int_fetch_add (p, incr);
#elif defined(__GNUC__)||defined(__INTEL_COMPILER)
  return __sync_fetch_and_add (p, incr);
#else
  int64_t out = *p;
  *p += incr;
  return out;
#endif
}

static inline int64_t
int64_compare_and_swap (int64_t * p, int64_t oldval, int64_t newval);

MTA("mta inline")
MTA("mta expect parallel")
int64_t
int64_compare_and_swap (int64_t * p, int64_t oldval, int64_t newval)
{
#if defined(__MTA__)
  int64_t t = *p;
  if (t == oldval) {
    t = readfe (p);
    if (t == oldval) {
      writeef (p, newval);
    } else {
      writeef (p, oldval);
    }
  }
  return t;
#elif defined(_OPENMP)&&(defined(__GNUC__)||defined(__INTEL_COMPILER))
  return __sync_val_compare_and_swap (p, oldval, newval);
#elif defined(_OPENMP)
#error "Atomics not defined..."
#else
  int64_t t = *p;
  if (t == oldval) *p = newval;
  return t;
#endif
}

/* In timer.c */
extern void init_timer (void);
extern void tic (void);
extern double toc (void);

/* {{{ Parameters and defaults */

/* Scale parameter for RMAT(scale), 2**scale vertices */
#define SCALE_DEFAULT 16
#define SCALE_MAX 40
static int scale = SCALE_DEFAULT;

/* Edge multiplier, edgefact * 2**scale edges in generated graph. */
#define EDGEFACT_DEFAULT 8
static int edgefact = EDGEFACT_DEFAULT;

/* RMAT probabilities for the quadrants [A, B; C, D]. */
#define PROB_A_DEFAULT 0.55
#define PROB_B_DEFAULT 0.1
#define PROB_C_DEFAULT 0.1
#define PROB_D_DEFAULT 0.25
static double prob_a = PROB_A_DEFAULT, prob_b = PROB_B_DEFAULT,
  prob_c = PROB_C_DEFAULT, prob_d = PROB_D_DEFAULT;

#define INITIAL_GRAPH_NAME_DEFAULT "graph-el.bin"
static const char * initial_graph_name = INITIAL_GRAPH_NAME_DEFAULT;

static int verbose = 0;
static int whole_graph = 1;
static int use_file_allocation = 0;

/* }}} */

/* {{{ Command line parsing */

static void
usage (FILE* out, char *progname)
{
  fprintf (out,
	   "%s [--verbose] [--scale=SCALE] [--stats=A,B,C,D] [--edgefact=EDGEFACT]\n"
	   "\t\t[--file-alloc] [initial-graph-el.bin]\n"
	   "\tDefaults:\n"
	   "\t   SCALE = %d, EDGEFACT = %d,\n"
	   "\t   A = %g, B = %g, C = %g, D = %g\n"
	   "\t   initial-graph name = \"%s\"\n",
#if !defined(__MTA__)
	   basename (progname),
#else
	   progname,
#endif
	   SCALE_DEFAULT, EDGEFACT_DEFAULT,
	   PROB_A_DEFAULT, PROB_B_DEFAULT, PROB_C_DEFAULT, PROB_D_DEFAULT,
	   INITIAL_GRAPH_NAME_DEFAULT);
}

static void
parse_args (const int argc, char *argv[])
{
  int arg_err = 0;
  verbose = (NULL != getenv ("VERBOSE"));
  while (1) {
    int c;
    static struct option long_options[] = {
      {"scale", required_argument, 0, 's'},
      {"edgefact", required_argument, 0, 'E'},
      {"stats", required_argument, 0, 'S'},
      {"component", no_argument, 0, 'C'},
      {"file-alloc", no_argument, 0, 'f'},
      {"verbose", no_argument, 0, 'v'},
      {"help", no_argument, 0, 'h'},
      {0, 0, 0, 0}
    };
    int option_index = 0;
    extern char *optarg;
    extern int optind, opterr, optopt;

    c = getopt_long (argc, argv, "s:E:a:d:D:S:fCvh?",
		     long_options, &option_index);
    if (-1 == c) break;
    switch (c) {
    case '?':
    case 'h':
      usage (stdout, argv[0]);
      exit (EXIT_SUCCESS);
    case 'v':
      verbose = 1;
      break;
    case 'C':
      whole_graph = 0;
      break;
    case 'f':
      use_file_allocation = 1;
      break;
    case 's':
      errno = 0;
      scale = strtol (optarg, NULL, 10);
      if (errno || scale < 0 || scale > SCALE_MAX) {
	fprintf (stderr, "Error: scale outside range {0, 1, ..., %d}: %s\n",
		 SCALE_MAX, optarg);
	arg_err = 1;
      }
      break;
    case 'E':
      errno = 0;
      edgefact = strtol (optarg, NULL, 10);
      if (errno || edgefact < 1) {
	fprintf (stderr, "Error: edgefact outside range {1, 2, ..., ∞}: %s\n",
		 optarg);
	arg_err = 1;
      }
      break;
    case 'S': {
      char* subarg = NULL;
      char* subarg_next = NULL;
      errno = 0;
      subarg = optarg;
      prob_a = strtod (subarg, &subarg_next);
      if (subarg == subarg_next) prob_a = -1.0;
      subarg = subarg_next;
      if (!isspace (*subarg)) ++subarg;
      prob_b = strtod (subarg, &subarg_next);
      if (subarg == subarg_next) prob_b = -1.0;
      subarg = subarg_next;
      if (!isspace (*subarg)) ++subarg;
      prob_c = strtod (subarg, &subarg_next);
      if (subarg == subarg_next) prob_c = -1.0;
      subarg = subarg_next;
      if (!isspace (*subarg)) ++subarg;
      prob_d = strtod (subarg, &subarg_next);
      if (subarg == subarg_next) prob_d = -1.0;
      subarg = subarg_next;
      if (errno) {
	fprintf (stderr, "Error parsing probability string %s\n",
		 optarg);
	arg_err = 1;
      }
      else {
	long double sum = 0;
	int nempty = 0;
	if (prob_a == -1) ++nempty;
	else if (prob_a < 0 || prob_a > 1) {
	  fprintf (stderr, "Error: A outside range [0, 1]: %s\n",
		   optarg);
	  arg_err = 1;
	}
	else sum += prob_a;
	if (prob_b == -1) ++nempty;
	else if (prob_b < 0 || prob_b > 1) {
	  fprintf (stderr, "Error: B outside range [0, 1]: %s\n",
		   optarg);
	  arg_err = 1;
	}
	else sum += prob_b;
	if (prob_c == -1) ++nempty;
	else if (prob_c < 0 || prob_c > 1) {
	  fprintf (stderr, "Error: C outside range [0, 1]: %s\n",
		   optarg);
	  arg_err = 1;
	}
	else sum += prob_c;
	if (prob_d == -1) ++nempty;
	else if (prob_d < 0 || prob_d > 1) {
	  fprintf (stderr, "Error: D outside range [0, 1]: %s\n",
		   optarg);
	  arg_err = 1;
	}
	else sum += prob_d;
	sum = (1.0 - sum) / nempty;
	if (prob_a == -1) prob_a = sum;
	if (prob_b == -1) prob_b = sum;
	if (prob_c == -1) prob_c = sum;
	if (prob_d == -1) prob_d = sum;
      }
      break;
    }
    default:
      arg_err = 1;
      break;
    }
  }
  if (arg_err) {
    usage (stderr, argv[0]);
    exit (EXIT_FAILURE);
  }

  if (optind < argc)
    initial_graph_name = argv[optind++];
}

/* }}} */

/* {{{ Output files */

#if !defined(__MTA__)
static int rmat_fd, action_fd;
#endif

static void
open_output_files (void)
{
#if !defined(__MTA__)
  mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

  rmat_fd = open (initial_graph_name, O_WRONLY|O_CREAT|O_TRUNC, mode);
  if (rmat_fd < 0) {
    perror ("Couldn't open output graph: ");
    abort ();
  }
#else /* __MTA__ inanity */
  extern void xmt_luc_io_init (void);
  xmt_luc_io_init ();
#endif
}

static ssize_t
xwrite(int fd, const void *buf, size_t len)
{
  ssize_t lenwritten;
  while (1) {
    lenwritten = write(fd, buf, len);
    if ((lenwritten < 0) && (errno == EAGAIN || errno == EINTR))
      continue;
    return lenwritten;
  }
}

static void
dump_el (int64_t nv, int64_t ne,
	 int64_t * base)
{
  size_t nwrite;
  const uint64_t endian_check = 0x1234ABCDul;

  base[0] = endian_check;
  base[1] = nv;
  base[2] = ne;
#if !defined(__MTA__)
#if 0
  nwrite = xwrite (rmat_fd, base, (3 + nv + 3*ne) * sizeof (int64_t));
  if (nwrite != (3 + nv + 3*ne) * sizeof (int64_t))
    goto err;
#else
  xwrite (rmat_fd, base, 3 * sizeof (int64_t));
  xwrite (rmat_fd, &base[3], nv * sizeof (int64_t));
  xwrite (rmat_fd, &base[3+nv], ne * sizeof (int64_t));
  xwrite (rmat_fd, &base[3+nv+ne], ne * sizeof (int64_t));
  xwrite (rmat_fd, &base[3+nv+2*ne], ne * sizeof (int64_t));
#endif
#else /* __MTA__ inanity */
  extern void xmt_luc_snapout (const char*, void*, size_t);
  xmt_luc_snapout (initial_graph_name, base, (3 + nv + 3*ne) * sizeof (int64_t));
#endif

  return;
#if 0 /* See above if 0 */
 err:
#endif
  perror ("Error writing initial rmat graph: ");
  abort ();    
}

static void
close_output (void)
{
#if !defined(__MTA__)
  if (rmat_fd >= 0) close (rmat_fd);
#endif
}

/* }}} */

#define I(EL_, K_) ((EL_)[3*(K_)])
#define J(EL_, K_) ((EL_)[1+3*(K_)])
#define W(EL_, K_) ((EL_)[2+3*(K_)])
#define Iel(EL_,K_) ((EL_)[0+3*(K_)])
#define Jel(EL_,K_) ((EL_)[1+3*(K_)])
#define Wel(EL_,K_) ((EL_)[2+3*(K_)])

/* Recursively divide a grid of N x N by four to a single point, (i, j).
   Choose between the four quadrants with probability a, b, c, and d.
   Create an edge between node i and j.
*/

MTA("mta expect parallel context")
MTA("mta inline")
static void
rmat_edge (int64_t *iout, int64_t *jout,
	   int SCALE, double A, double B, double C, double D,
	   const double *rn)
{
  static int xxx = 0;
  size_t rni = 0;
  int64_t i = 0, j = 0;
  int64_t bit = ((int64_t)1) << (SCALE-1);

  while (1) {
    const double r = rn[rni++];
    if (r > A) { /* outside quadrant 1 */
      if (r <= A + B) /* in quadrant 2 */
	j |= bit;
      else if (r <= A + B + C) /* in quadrant 3 */
	i |= bit;
      else { /* in quadrant 4 */
	j |= bit;
	i |= bit;
      }
    }
    if (1 == bit) break;

    /*
      Assuming R is in (0, 1), 0.95 + 0.1 * R is in (0.95, 1.05).
      So the new probabilities are *not* the old +/- 10% but
      instead the old +/- 5%.
    */
    A *= (9.5 + rn[rni++])/10;
    B *= (9.5 + rn[rni++])/10;
    C *= (9.5 + rn[rni++])/10;
    D *= (9.5 + rn[rni++])/10;
    /* Used 5 random numbers. */

    {
      const double norm = 1.0 / (A + B + C + D);
      A *= norm; B *= norm; C *= norm;
    }
    /* So long as +/- are monotonic, ensure a+b+c+d <= 1.0 */
    D = 1.0 - (A + B + C);
	
    bit >>= 1;
  }
  /* Iterates SCALE times. */
  *iout = i;
  *jout = j;
}

MTA("mta expect serial context")
static void
rmat_edge_list (size_t listlen,
		int64_t * restrict el,
		int SCALE, double A, double B, double C, double D,
		size_t nwork, double * work)
{
  size_t nrand;
  double * restrict urand = work;
  size_t k;

  /* Needs 5*SCALE random numbers per edge. */
  nrand = listlen * 5 * SCALE;
  assert (nrand <= nwork);

  OMP("omp parallel for")
    MTA("mta assert nodep")
    MTASTREAMS()
    for (size_t k = 0; k < listlen; ++k) {
      int64_t i, j;
      rmat_edge (&i, &j, SCALE, A, B, C, D, &urand[k*5*SCALE]);
      if (i < j) { int64_t t = i; i = j; j = t; }
      I(el, k) = i;
      J(el, k) = j;
      W(el, k) = 1;
      assert (i >= j);
    }
}

static int64_t
largest_connected_component (int64_t * restrict D,
			     int64_t nv, int64_t ne, int64_t * restrict el,
			     int64_t * restrict ws)
{
  int64_t new_nv = 0;
  int64_t nchanged;
  int64_t largest = -1, largest_i = -1;
  int64_t * restrict cnt = ws;
  int64_t ncomm = 0;
	
  OMP("omp parallel") {
    int64_t step = 0;
    OMP("omp for")
      for (int64_t i = 0; i < nv; ++i) {
	D[i] = i;
	cnt[i] = 0;
      }

    while (1) {
      OMP("omp single") nchanged = 0;
      OMP("omp barrier");
      MTA("mta assert nodep") OMP("omp for reduction(+:nchanged)")
	for (int64_t k = 0; k < ne; ++k) {
	  const int64_t i = I(el, k);
	  const int64_t j = J(el, k);
	  assert (i >= j);
	  if (D[j] < D[i] && D[i] == D[D[i]]) {
	    D[D[i]] = D[j];
	    ++nchanged;
	  }
	}
      if (!nchanged) break;
      MTA("mta assert nodep") OMP("omp for")
	for (int64_t i = 0; i < nv; ++i) {
	  while (D[i] != D[D[i]])
	    D[i] = D[D[i]];
	}
    }

    MTA("mta assert nodep") OMP("omp for reduction(+:ncomm)")
      for (int64_t i = 0; i < nv; ++i) {
	if (i == D[i]) ++ncomm;
	while (D[i] != D[D[i]])
	  D[i] = D[D[i]];
	int64_fetch_add (&cnt[D[i]], 1);
      }

    /* fprintf (stderr, "%d: ncomm %ld\n", omp_get_thread_num (), ncomm); */

    {
      int64_t local_max_i = -1;
      int64_t local_max = 0;
      MTA("mta assert nodep") OMP("omp for")
	for (int64_t i = 0; i < nv; ++i)
	  if (i == D[i] && cnt[i] > local_max) {
	    local_max = cnt[i];
	    local_max_i = i;
	  }
      OMP("omp critical")
	if (local_max > largest) {
	  largest = local_max;
	  largest_i = local_max_i;
	}
      OMP("omp barrier");
    }
    OMP("omp flush (largest, largest_i)");

    /* fprintf (stderr, "%d: largest_i %ld  size %ld\n", omp_get_thread_num (), */
    /* 	     (long)largest_i, (long)largest); */
    /* OMP("omp master") { */
    /*   fprintf (stderr, "parent:"); */
    /*   for (int64_t i = 0; i < nv; ++i) { */
    /* 	fprintf (stderr, " %ld:%ld", (long)i, (long)D[i]); */
    /*   } */
    /*   fprintf (stderr, "\n"); */
    /* } */

    MTA("mta assert nodep") OMP("omp for")
      for (int64_t i = 0; i < nv; ++i) {
	if (D[i] == largest_i)
	  D[i] = int64_fetch_add (&new_nv, 1);
	else
	  D[i] = -1;
      }
  }

  /* fprintf (stderr, "mapping:"); */
  /* for (int64_t i = 0; i < nv; ++i) { */
  /*   fprintf (stderr, " %ld:%ld", (long)i, (long)D[i]); */
  /* } */
  /* fprintf (stderr, "\n"); */

  return new_nv;
}

MTA("mta inline")
MTA("mta expect parallel context")
static inline uint64_t
hash (int64_t i, int64_t j)
{
#if 0
  /* FNV hash http://www.isthe.com/chongo/tech/comp/fnv/#FNV-1a */
  const uint64_t FNV_prime = 1099511628211ul;
  uint64_t out = 14695981039346656037ul;

  for (int k = 0; k < 8; ++k) {
    out = out ^ (i & 0xFF);
    out *= FNV_prime;
    i >>= 8;
    out = out ^ (j & 0xFF);
    out *= FNV_prime;
    j >>= 8;
  }

  return out;
#elif 0
  uint64_t out = 0;
  for (int k = 0; k < 8; ++k) {
    out = (out << 4) ^ (out >> 28) ^ (i & 0xFF);
    i >>= 8;
    out = (out << 4) ^ (out >> 28) ^ (j & 0xFF);
    j >>= 8;
  }
  return out;
#else
  return i ^ j;
#endif
}

static int64_t
contract_el (int64_t NE, int64_t * restrict el /* 3 x oldNE */,
	     int64_t old_nv,
	     int64_t new_nv,
	     int64_t * restrict d /* oldNV */,
	     const int64_t * restrict m /* oldNV */,
	     int64_t * restrict ws /* oldNV + oldNE */)
{
  int64_t n_new_edges = 0;

#if !defined(NDEBUG)
  int64_t w_in = 0, w_out = 0, w_dropped = 0;
#endif

  int64_t nbuckets = old_nv;
  int64_t tail;

  int64_t * restrict next = ws;
  int64_t * restrict head = &next[NE];

  OMP("omp parallel") {
#if !defined(NDEBUG)
    OMP("omp for reduction(+:w_in)")
      for (int64_t i = 0; i < old_nv; ++i)
	w_in += d[i];
    OMP("omp for reduction(+:w_in)")
      for (int64_t k = 0; k < NE; ++k)
	w_in += Wel(el, k);
#endif

    OMP("omp for")
      for (int64_t i = 0; i < new_nv; ++i)
	next[i] = 0;
    OMP("omp for")
      for (int64_t old_i = 0; old_i < old_nv; ++old_i) {
	const int64_t new_i = m[old_i];
	if (new_i >= 0)
	  OMP("omp atomic") MTA("mta update") next[new_i] += d[old_i];
      }
    OMP("omp for")
      for (int64_t i = 0; i < new_nv; ++i)
	d[i] = next[i];

#if !defined(NDEBUG)
    OMP("omp for reduction(+:w_out)")
      for (int64_t i = 0; i < new_nv; ++i)
	w_out += d[i];
    OMP("omp for reduction(+:w_out)")
      for (int64_t k = 0; k < NE; ++k)
	w_out += Wel(el, k);
    OMP("omp single")
      if (w_in != w_out)
	fprintf (stderr, "%ld %ld\n", (long)w_in, (long)w_out);
    OMP("omp barrier");
    assert (w_out == w_in);
    OMP("omp barrier");
    OMP("omp single") w_out = 0;
#endif

    OMP("omp for")
      for (int64_t i = 0; i < nbuckets; ++i)
	head[i] = -1;
    OMP("omp for")
      for (int64_t k = 0; k < NE; ++k)
	next[k] = -2;

    OMP("omp for") MTA("mta assert nodep")
      for (int64_t k = 0; k < NE; ++k) {
	int64_t old_i = Iel(el, k);
	int64_t old_j = Jel(el, k);
	int64_t new_i = m[Iel(el, k)];
	int64_t new_j = m[Jel(el, k)];
#if !defined(NDEBUG)
	if (new_i >= new_nv || old_i >= old_nv)
	  fprintf (stderr, "new_i %ld: (%ld, %ld) / %ld -> (%ld, %ld) / %ld\n",
		   (long)k,
		   (long)old_i, (long)old_i,
		   (long)old_nv,
		   (long)new_i, (long)new_j,
		   (long)new_nv);
#endif
		   assert (old_i < old_nv);
		   assert (old_j < old_nv);
		   assert (new_i < new_nv);
		   assert (new_j < new_nv);
		   if (new_i >= 0 && new_j >= 0) { /* Permit simple dropping. */
		     if (new_i == new_j) {
		       assert (new_i >= 0);
		       assert (new_i < new_nv);
		       /* Accumulate into diagonal. */
		       int64_fetch_add (&d[new_i], Wel(el, k));
		       assert (Wel(el,k) >= 0);
		       next[k] = -2; /* Delete. */
		     } else {	
		       /* Choose a head, somewhat hashed across [0, old_nv). */
		       //int64_t key; /* = hash (new_i, new_j); % nbuckets;*/
		       int64_t pk;
		       int64_t *p;
		       int64_t key = hash (new_i, new_j);
		       key ^= key >> 32;
		       key %= nbuckets;
		       if (new_i < new_j) {
			 /* Swap into lower-triangle. */
			 int64_t t = new_j;
			 new_j = new_i;
			 new_i = t;
		       }
		       assert (new_i > new_j);

		       /* Walk the list starting at head[key]. */

		       p = &head[key];
		       pk = head[key];

		       if (pk < 0) { /* Might be an empty bucket. */
#if defined(__MTA__)||!defined(_OPENMP)
			 pk = readfe (p);
			 if (pk < 0) { /* Definitely an empty bucket. */
			   Iel(el, k) = new_i; /* Use new labels. */
			   Jel(el, k) = new_j;
			   next[k] = -1;
			   pk = k;
			   MTA("mta update") ++n_new_edges;
			 }
			 writeef (p, pk);
#else
			 int64_t t;
			 assert (pk == -1);
			 Iel(el, k) = new_i; /* Use new labels. */
			 Jel(el, k) = new_j;
			 next[k] = -1;
			 t = int64_compare_and_swap (p, pk, k);
			 if (t == pk) { /* grabbed it */
			   pk = k;
			   OMP("omp atomic") ++n_new_edges;
			 } else {
			   pk = t;
			   next[k] = -2;
			 }
#endif
		       }

		       while (pk != k && pk >= 0) {
			 if (new_i == Iel(el, pk) && new_j == Jel(el, pk)) {
			   assert (next[pk] >= -1);
			   assert (next[k] == -2);
			   //Wel(el, pk) += Wel(el, k);
			   OMP("omp atomic") MTA("mta update") Wel(el, pk) += Wel(el, k);
			   //int64_fetch_add (&Wel(el, pk), Wel(el, k));
			   break;
			 }

			 p = &next[pk];
			 pk = next[pk];
			 assert (pk >= -1);

			 if (pk < 0) { /* Might be at tail... */
#if defined(__MTA__)||!defined(_OPENMP)
			   pk = readfe (p);
			   if (pk == -1) { /* Newly seen edge. */
			     assert (-1 == *p);
			     pk = k; /* Append to list. */
			     Iel(el, k) = new_i; /* Use new labels. */
			     Jel(el, k) = new_j;
			     next[k] = -1;
			     MTA("mta update") OMP("omp atomic") ++n_new_edges;
			   }
			   writeef (p, pk);
#else
			   int64_t t;
			   assert (pk == -1);
			   Iel(el, k) = new_i; /* Use new labels. */
			   Jel(el, k) = new_j;
			   next[k] = -1;
			   t = int64_compare_and_swap (p, pk, k);
			   if (t == pk) { /* grabbed it */
			     pk = k;
			     OMP("omp atomic") ++n_new_edges;
			   } else {
			     pk = t;
			     next[k] = -2;
			   }
#endif
			 }
		       }
		     }
		   } else {
		     /* fprintf (stderr, "foo (%ld,%ld;%ld) -> (%ld,%ld)\n", */
		     /* 	      (long)Iel(el,k), (long)Jel(el,k), (long)Wel(el,k), */
		     /* 	      (long)new_i, (long)new_j); */
#if !defined(NDEBUG)
		     OMP("omp atomic") w_dropped += Wel(el,k);
#endif
		     /* Drop by setting next to -2. */
		     next[k] = -2;
		   }
      }

#if !defined(NDEBUG)
    OMP("omp for reduction(+:w_out)")
      for (int64_t i = 0; i < new_nv; ++i)
	w_out += d[i];
    OMP("omp for reduction(+:w_out)")
      for (int64_t k = 0; k < NE; ++k)
	if (next[k] != -2)
	  w_out += Wel(el, k);
    OMP("omp single")
      if (w_in != w_out+w_dropped)
	fprintf (stderr, "%ld %ld %ld\n", (long)w_in, (long)w_out, (long)w_dropped);
    OMP("omp barrier");
    assert (w_out+w_dropped == w_in);
    OMP("omp barrier");
    OMP("omp single") w_out = 0;
#endif

    OMP("omp single") tail = n_new_edges;
    OMP("omp barrier");
    /* Pack unique edges into free space within the first n_new_edges slots. */
    OMP("omp for") MTA("mta assert nodep")
      for (int64_t k = 0; k < n_new_edges; ++k) {
	if (next[k] == -2) {
	  /* A deleted slot... */
	  int64_t kfrom;
 	  do { /* Find first non-deleted slot after n_new_edges */
	    kfrom = int64_fetch_add (&tail, 1);
	    assert (kfrom < NE);
	  } while (next[kfrom] <= -2);
#if !defined(NDEBUG)
	  if (!(next[kfrom] >= -1)) {
	    fprintf (stderr, "%d: XXX (%ld,%ld;%ld) [%ld]==%ld\n",
#if defined(_OPENMP)
		     omp_get_thread_num (),
#else
		     1,
#endif
		     (long)Iel(el,kfrom), (long)Jel(el,kfrom), (long)Wel(el,kfrom),
		     (long)kfrom, (long)next[kfrom]);
	  }
	  assert (next[kfrom] >= -1);
#endif
	  Iel(el, k) = Iel(el, kfrom);
	  Jel(el, k) = Jel(el, kfrom);
	  Wel(el, k) = Wel(el, kfrom);
	  assert (Wel(el, k));
	  next[k] = 2*NE;
#if defined(_OPENMP)&&!defined(NDEBUG)
	  next[kfrom] = -2 - omp_get_thread_num ();
#else
	  next[kfrom] = -2;
#endif
	}
	assert (next[k] >= -1);
      }

    /* fprintf (stderr, "%d: tail %ld   n_new_edges %ld / %ld\n", omp_get_thread_num (), */
    /* 	       (long)tail, (long)n_new_edges, (long)NE); */

#if !defined(NDEBUG)
    assert (n_new_edges <= NE);
    OMP("omp for")
      for (int64_t k = 0; k < n_new_edges; ++k)
	assert (next[k] >= -1);
    OMP("omp for")
      for (int64_t k = n_new_edges; k < NE; ++k) {
	if (next[k] >= -1) {
	  fprintf (stderr, "next[%ld] == %ld >= -1\n", (long)k, (long)next[k]);
	  fprintf (stderr, "    (%ld, %ld; %ld)\n",
		   (long)Iel(el, k), (long)Jel(el, k), (long)Wel(el, k));
	  for (int64_t k2 = 0; k2 < n_new_edges; ++k2) {
	    if (Iel(el, k) == Iel(el, k2) && Jel(el, k) == Jel(el, k2)) {
	      fprintf (stderr, "next[%ld] == %ld >= -1\n", (long)k, (long)next[k]);
	      fprintf (stderr, "    (%ld, %ld; %ld)\n",
		       (long)Iel(el, k2), (long)Jel(el, k2), (long)Wel(el, k2));
	      
	    }
	  }
	}
    	assert (next[k] < -1);
      }
    OMP("omp for")
      for (int64_t k = tail; k < NE; ++k) {
	if (next[k] >= -1) {
	  fprintf (stderr, "(tail) next[%ld] == %ld >= -1\n", (long)k, (long)next[k]);
	  fprintf (stderr, "(tail)    (%ld, %ld; %ld)\n",
		   (long)Iel(el, k), (long)Jel(el, k), (long)Wel(el, k));
	}
    	assert (next[k] < -1);
      }

    OMP("omp for reduction(+:w_out)")
      for (int64_t i = 0; i < new_nv; ++i)
	w_out += d[i];
    OMP("omp for reduction(+:w_out)")
      for (int64_t k = 0; k < n_new_edges; ++k)
	w_out += Wel(el, k);
    OMP("omp single") {
      if (w_in != w_out+w_dropped)
	fprintf (stderr, "%ld %ld %ld\n", (long)w_in, (long)w_out, (long)w_dropped);
      assert (w_in == w_out+w_dropped);
    }
#endif
  }

  return n_new_edges;
}

int
main (int argc, char ** argv)
{
  int64_t *base;

  int64_t nv, ne, orig_nv, orig_ne;
  int64_t *el, *d;

  size_t niwork;
  int64_t *iwork;

  double rmat_time;

  int64_t listlen_max;
  size_t alloc_sz;
  parse_args (argc, argv);
  open_output_files ();
  init_urandom ();
  init_timer ();

  orig_nv = nv = ((int64_t)1) << scale;
  orig_ne = ne = nv * edgefact;
  printf ("nv: %ld, ne: %ld\n", (long)nv, (long)ne);

  niwork = 2*nv + ne;
  printf ("niwork: %ld\n", (long)niwork);
  listlen_max = niwork / (5 * scale);
  if (listlen_max < 4096) {
    niwork = 4096 * 5 * scale;
    listlen_max = 4096;
  }
  printf ("listlen: %ld\n", (long)listlen_max);
  alloc_sz = 3 * sizeof (nv) + nv * sizeof (*d) + 3 * ne * sizeof (*el) +
    niwork * sizeof (*iwork);
  if (!use_file_allocation)
    base = xmalloc (alloc_sz);
  else
    base = xmmap_file_alloc (alloc_sz);
  d = &base[3];
  el = &d[nv];
  iwork = &el[3*ne];

  /* Generate static RMAT, largest component */
  tic ();
  if (verbose) { printf ("generating edge list ..."); fflush (stdout); }
  for (int64_t k = 0; k < ne; k += listlen_max) {
    int64_t listlen = listlen_max;
    /* if (verbose) { printf (" %ld", (long)k); fflush (stdout); } */
    if (k + listlen > ne) listlen = ne - k;
    uniform_random (5 * scale * listlen, (double*)iwork);
    /* if (verbose) { printf ("(rnd)"); fflush (stdout); } */
    rmat_edge_list (listlen, &el[3*k], scale, prob_a, prob_b, prob_c, prob_d,
    		    5 * scale * listlen, (double*)iwork);
    /* if (verbose) { printf ("(el)"); fflush (stdout); } */
  }
  /* MTA("mta assert parallel") /\* WTF? *\/ */
  /*   MTA("mta use 100 streams") */
  /*   for (int64_t k = 0; k < ne; k++) */
  /*     W(el, k) = 1; */
  if (verbose) {
    printf (" done: nv = %ld, ne = %ld\n", (long)nv, (long)ne);
    fflush (stdout);
  }
  /* fprintf (stderr, "generated list\n"); */
  if (!whole_graph) {
    if (verbose) { printf ("finding largest component ..."); fflush (stdout); }
    nv = largest_connected_component (iwork, nv, ne, el, &iwork[orig_nv]);
    if (verbose) {
      printf (" done: nv = %ld, ne = %ld\n", (long)nv, (long)ne);
      fflush (stdout);
    }
  } else {
    nv = orig_nv;
    OMP("omp parallel for") MTA("mta assert parallel")
      for (int64_t i = 0; i < nv; ++i) iwork[i] = i;
  }
  /* fprintf (stderr, "connected comp. map\n"); */
  if (verbose) { printf ("contracting ..."); fflush (stdout); }
  ne = contract_el (ne, el, orig_nv, nv, d, iwork, &iwork[orig_nv]);
  if (verbose) {
    printf (" done: nv = %ld, ne = %ld\n", (long)nv, (long)ne);
    fflush (stdout);
  }
  /* fprintf (stderr, "contracted\n"); */
  rmat_time = toc ();
  printf ("time rmat: %g\n", rmat_time);
  printf ("nv: %ld, ne: %ld\n", (long)nv, (long)ne);

  memmove (&d[nv], el, 3 * ne * sizeof (*el));
  el = &d[nv];

  dump_el (nv, ne, base);

  close_output ();

  if (!use_file_allocation)
    free (base);
  else
    xmunmap (base, alloc_sz);

  return EXIT_SUCCESS;
}
