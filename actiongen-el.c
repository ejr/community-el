#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <string.h>

#include <errno.h>
#include <assert.h>

#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>

#include "compat.h"
#include "xmalloc.h"
#include "prng.h"
#include "xmt-luc.h"
#include "graph-el.h"

/*
  Goal: evaluate alg performance, not be realistic
*/

MTA("mta inline")
MTA("mta expect parallel context")
static double dprefix_sum (const int64_t n, double *ary);

MTA("mta inline")
MTA("mta expect parallel context")
static int64_t bs64 (int64_t)
#if defined(__GNUC__)
    __attribute__((const))
#endif
    ;

static void bs64_n (size_t, int64_t * restrict);

static struct el g;

static int64_t ncomm, totw;
static int64_t * restrict c;
static int64_t * restrict volc;
static int64_t * restrict extvolc;
static int64_t * restrict nvc;
static int64_t * restrict deg;
static int64_t * restrict extdeg;

static double * restrict Pcomm;
static double * restrict Pvtx_int;
static double * restrict Pvtx_ext;

static int64_t ndelq;
static int64_t * restrict delq;

static int64_t nacts = 100000;
static const double DELQ_PROB = 0.0625;
static int64_t * actq_mem;
static int64_t * restrict actq;

static void snarf_cmap (const int64_t, const char *, const int, int64_t **);

int
main (int argc, char **argv)
{
  int needs_bs;

  init_urandom ();

  if (!argv[1] || !argv[2] || !argv[3]) {
    fprintf (stderr, "Usage: %s elbin cmap outfile\n", argv[0]);
    return EXIT_FAILURE;
  }
  if (getenv ("NACTS")) {
    nacts = strtol (getenv ("NACTS"), NULL, 10);
  }

  needs_bs = snarf_graph (argv[1], &g);
  snarf_cmap (g.nv, argv[2], needs_bs, (int64_t**)&c); /* Assumes same byte order. */
  ncomm = 1;

  OMP("omp parallel") {
    int64_t ncomm_local = 1;
    OMP("omp for nowait")
      for (int64_t k = 0; k < g.nv; ++k)
	if (c[k]+1 > ncomm_local) ncomm_local = c[k]+1;
    OMP("omp critical")
      if (ncomm_local > ncomm) ncomm = ncomm_local;;
  }

  /* Accumulate degrees and volumes. */
  volc /* ncomm */ = xcalloc (3*ncomm + 2*g.nv, sizeof (int64_t));
  extvolc /* ncomm */ = &volc[ncomm];
  nvc /* ncomm */ = &extvolc[ncomm];
  deg /* g.nv */ = &nvc[ncomm];
  extdeg /* g.nv */ = &deg[g.nv];

  Pcomm /* ncomm */ = xcalloc (ncomm + 2*g.nv, sizeof (double));
  Pvtx_int /* g.nv */ = &Pcomm[ncomm];
  Pvtx_ext /* g.nv */ = &Pvtx_int[g.nv];
  double Pcomm_tot = 0, Pvtx_int_tot = 0, Pvtx_ext_tot = 0;

  totw = 0;
  OMP("omp parallel") {
    CDECL(g);
    OMP("omp for nowait reduction(+: totw)")
      for (int64_t k = 0; k < g.nv; ++k) {
	const int64_t w = D(g, k);
	const int64_t ck = c[k];
	OMP("omp atomic") ++nvc[ck];
	if (w) {
	  OMP("omp atomic")
	    deg[k] += w;
	  OMP("omp atomic")
	    volc[ck] += w;
	  totw += w;
	}
      }
    OMP("omp for reduction(+: totw)")
      for (int64_t k = 0; k < g.ne; ++k) {
	const int64_t i = I(g, k);
	const int64_t ci = c[i];
	const int64_t j = I(g, k);
	const int64_t cj = c[i];
	const int64_t w = I(g, k);
	OMP("omp atomic")
	  deg[i] += w;
	OMP("omp atomic")
	  deg[j] += w;
	OMP("omp atomic")
	  volc[ci] += w;
	OMP("omp atomic")
	  volc[cj] += w;
	if (ci != cj) {
	  OMP("omp atomic")
	    extvolc[cj] += w;
	  OMP("omp atomic")
	    extvolc[cj] += w;
	}
	totw += 2*w;
      }

    OMP("omp for nowait reduction(+: Pcomm_tot)")
      for (int64_t k = 0; k < ncomm; ++k) {
	Pcomm[k] = nvc[k] / (double)volc[k];
	Pcomm_tot += Pcomm[k];
      }

    OMP("omp for nowait reduction(+: Pvtx_int_tot) reduction(+: Pvtx_ext_tot)")
      for (int64_t k = 0; k < g.nv; ++k) {
	const int64_t ck = c[k];
	Pvtx_int[k] = (volc[k] - extvolc[k]) / (double)deg[k];
	Pvtx_ext[k] = deg[k] / (double)extvolc[ck];
	Pvtx_int_tot += Pvtx_int[k];
	Pvtx_ext_tot += Pvtx_ext[k];
      }

    OMP("omp barrier");

    OMP("omp for nowait")
      for (int64_t k = 0; k < ncomm; ++k)
	Pcomm[k] /= Pcomm_tot;

    OMP("omp for nowait")
      for (int64_t k = 0; k < g.nv; ++k) {
	if (Pvtx_int_tot)
	  Pvtx_int[k] /= Pvtx_int_tot;
	else
	  Pvtx_int[k] = 1.0 / nvc[k];
	if (Pvtx_ext_tot)
	  Pvtx_ext[k] /= Pvtx_ext_tot;
	else
	  Pvtx_ext[k] = 1.0 / g.nv;
      }

    OMP("omp barrier");

    double tst1, tst2, tst3;
    tst1 = dprefix_sum (ncomm, Pcomm);
    tst2 = dprefix_sum (g.nv, Pvtx_int);
    tst3 = dprefix_sum (g.nv, Pvtx_ext);
  }

  ndelq = 0;
  delq = xmalloc (2 * g.nv * sizeof (int64_t));  /* Reasonable max queue size */
  /* For each edge, toss a coin and see if it lands in the removal queue. */
  {
    CDECL(g);
    double * restrict Pscratch = xmalloc (nacts * sizeof (*Pscratch));
    uniform_random (nacts, Pscratch);
    OMP("omp parallel for")
      for (int64_t k = 0; k < g.ne; ++k) {
	if (Pscratch[k] < DELQ_PROB) {
	  int64_t where = int64_fetch_add (&ndelq, 1);
	  if (where < g.nv) {
	    delq[2*where] = I(g, k);
	    delq[1+2*where] = J(g, k);
	  }
	}
      }
    free (Pscratch);
    if (ndelq > nacts) ndelq = nacts;
  }

  actq_mem = xmalloc (2 * (1+nacts) * sizeof (*actq_mem));
  actq_mem[0] = 0x1234ABCDul;
  actq_mem[1] = nacts;
  actq = &actq_mem[2];
  double * Pscratch = xmalloc (5 * nacts * sizeof (*Pscratch));
  uniform_random (5 * nacts, Pscratch);
  OMP("omp parallel for")
    for (int64_t k = 0; k < nacts; ++k) {
      if (Pscratch[5*k] < DELQ_PROB) {
	actq[2*k] = -1; /* Flag as deletion, next pass. */
      } else {
	const double PCw = Pscratch[1+5*k];
	const double PCu = Pscratch[2+5*k];
	const double Pw = Pscratch[3+5*k];
	const double Pu = Pscratch[4+5*k];

	int64_t Cw, Cu, w, u;

	/* yeah, should be bisection. */
	for (int64_t k2 = ncomm; k2 > 0; --k2)
	  if (Pcomm[k2-1] <= PCw) {
	    Cw = k2;
	    break;
	  }
	for (int64_t k2 = ncomm; k2 > 0; --k2)
	  if (Pcomm[k] <= PCu) {
	    Cu = k2;
	    break;
	  }
	if (Cu == Cw) {
	  for (int64_t k2 = g.nv; k2 > 0; --k2)
	    if (Pvtx_int[k2-1] <= Pw) {
	      w = k2;
	      break;
	    }
	  for (int64_t k2 = ncomm; k2 > 0; --k2)
	    if (Pvtx_int[k2-1] <= Pu) {
	      u = k2;
	      break;
	    }
	} else { /* Cu != Cw */
	  for (int64_t k2 = g.nv; k2 > 0; --k2)
	    if (Pvtx_ext[k2-1] <= Pw) {
	      w = k2;
	      break;
	    }
	  for (int64_t k2 = ncomm; k2 > 0; --k2)
	    if (Pvtx_ext[k2-1] <= Pu) {
	      u = k2;
	      break;
	    }
	}
	actq[2*k] = u;
	actq[1+2*k] = w;
      }
    }

  uniform_random (nacts, Pscratch);
  OMP("omp parallel for")
    for (int64_t k = 0; k < nacts; ++k)
      if (actq[2*k] < 0) {
	/* A deletion to fill. */
	int64_t which = -1;
	/* First, try the saved queue... */
	if (ndelq) {
	  int64_t which = int64_fetch_add (&ndelq, -1) - 1;
	  if (which >= 0) {
	    actq[2*k] = -(delq[2*which] + 1);
	    actq[1+2*k] = -(delq[1+2*which] + 1);
	  }
	}
	/* If none left, pick an earlier insertion. */
	if (which < 0) {
	  which = floor (Pscratch[k] * k);
	  while (which >= 0 && actq[2*which] < 0)
	    --which;
	  if (which < 0) {
	    fprintf (stderr, "augh, failed to find earlier insertion\n");
	    abort ();
	  }
	  actq[2*k] = -(actq[2*which] + 1);
	  actq[1+2*k] = -(actq[1+2*which] + 1);
	}
      }

  xmt_luc_snapout (argv[3], actq_mem, 2*(1+nacts)*sizeof (*actq_mem));

  return EXIT_SUCCESS;
}

int64_t
bs64 (int64_t xin)
{
    uint64_t x = (uint64_t)xin; /* avoid sign-extension issues */
    x = (x >> 32) | (x << 32);
    x = ((x & ((uint64_t)0xFFFF0000FFFF0000ull)) >> 16)
	| ((x & ((uint64_t)0x0000FFFF0000FFFFull)) << 16);
    x = ((x & ((uint64_t)0xFF00FF00FF00FF00ull)) >> 8)
	| ((x & ((uint64_t)0x00FF00FF00FF00FFull)) << 8);
    return x;
}

static void
bs64_n (size_t n, int64_t * restrict d)
{
  OMP("omp parallel for")
  MTA("mta assert nodep")
  for (size_t k = 0; k < n; ++k)
    d[k] = bs64 (d[k]);
}

void
snarf_cmap (const int64_t nv, const char *cfile, const int needs_bs,
	    int64_t **cout)
{
  int64_t * restrict c;

  assert (cfile);
  c = xmalloc (nv * sizeof (int64_t));
  xmt_luc_snapin (cfile, c, nv * sizeof (int64_t));
  if (needs_bs)
    bs64_n (nv, c);
  *cout = c;
}


#if defined(_OPENMP)
static double *buf;

double
dprefix_sum (const int64_t n, double *ary)
{
  int nt, tid;
  int64_t slice_begin, slice_end, t1, t2, k;
  double dt1, tmp;

  nt = omp_get_num_threads ();
  tid = omp_get_thread_num ();
  OMP("omp master")
    buf = xmalloc (nt * sizeof (*buf)); /* xlc hates alloca */
  OMP("omp flush (buf)");
  OMP("omp barrier");

  t1 = n / nt;
  t2 = n % nt;
  slice_begin = t1 * tid + (tid < t2? tid : t2);
  slice_end = t1 * (tid+1) + ((tid+1) < t2? (tid+1) : t2);

  tmp = 0;
  for (k = slice_begin; k < slice_end; ++k)
    tmp += ary[k];
  buf[tid] = tmp;
  OMP("omp barrier");
  OMP("omp single")
    for (k = 1; k < nt; ++k)
      buf[k] += buf[k-1];
  OMP("omp barrier");
  tmp = buf[nt-1];
  if (tid)
    dt1 = buf[tid-1];
  else
    dt1 = 0;
  for (k = slice_begin; k < slice_end; ++k) {
    double t = ary[k];
    ary[k] = dt1;
    dt1 += t;
  }
  OMP("omp barrier");

  OMP("omp master") {
    free (buf);
    buf = 0;
  }
  return t1;
}
#else
MTA("mta inline")
double
dprefix_sum (const int64_t n, double *ary)
{
  double cur = 0;
  for (int64_t k = 0; k < n; ++k) {
    double tmp = ary[k];
    ary[k] = cur;
    cur += tmp;
  }
  return cur;
}
#endif
