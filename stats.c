#define _POSIX_C_SOURCE 199309L
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#if defined(__MTA__)
#define MTA(x) _Pragma(x)
#else
#define MTA(x)
#endif

struct stats {
  char statname[257];
#if defined(__MTA__)
  double time;
  int64_t clock, issues, concurrency, load, store, ifa;
#else
  double time;
#endif
};

static int needs_init = 1;
static struct stats stats_tic_data, stats_toc_data;
static double timer_tic_data;

#ifdef __MTA__
#include <sys/mta_task.h>
#include <machine/runtime.h>
double timer(void)
{
MTA ("mta fence")
  return((double)mta_get_clock(0) / mta_clock_freq());
}

static int load_ctr = -1, store_ctr, ifa_ctr;

static void
init_stats (void)
{
  load_ctr = mta_rt_reserve_task_event_counter (3, RT_LOAD);
  assert (load_ctr >= 0);
  store_ctr = mta_rt_reserve_task_event_counter (2, RT_STORE);
  assert (store_ctr >= 0);
  ifa_ctr = mta_rt_reserve_task_event_counter (1, RT_INT_FETCH_ADD);
  assert (ifa_ctr >= 0);
  memset (&stats_tic_data, 0, sizeof (stats_tic_data));
  memset (&stats_toc_data, 0, sizeof (stats_toc_data));
  needs_init = 0;
}

static void
get_stats (struct stats *s)
{
MTA("mta fence")
  s->time = timer ();
MTA("mta fence")
  s->clock = mta_get_task_counter(RT_CLOCK);
MTA("mta fence")
  s->issues = mta_get_task_counter(RT_ISSUES);
MTA("mta fence")
  s->concurrency = mta_get_task_counter(RT_CONCURRENCY);
MTA("mta fence")
  s->load = mta_get_task_counter(RT_LOAD);
MTA("mta fence")
  s->store = mta_get_task_counter(RT_STORE);
MTA("mta fence")
  s->ifa = mta_get_task_counter(RT_INT_FETCH_ADD);
MTA("mta fence")
}

void
print_stats (FILE *out)
{
  if (needs_init) return;
  fprintf (out, "alg : %s\n", stats_tic_data.statname);
  fprintf (out, "time : %20.17g\n", stats_toc_data.time - stats_tic_data.time);
  fprintf (out, "teams : %d\n", mta_get_max_teams ());
#define PRINT(v) do { fprintf (out, #v " : %ld\n", (stats_toc_data.v - stats_tic_data.v)); } while (0)
  PRINT (clock);
  PRINT (issues);
  PRINT (concurrency);
  PRINT (load);
  PRINT (store);
  PRINT (ifa);
#undef PRINT
}

#else
#include <sys/time.h>
#include <time.h>
double timer(void)
{
    struct timespec tp;
/* #if defined(CLOCK_PROCESS_CPUTIME_ID) */
/* #define CLKID CLOCK_PROCESS_CPUTIME_ID */
/* #elif  defined(CLOCK_REALTIME_ID) */
#define CLKID CLOCK_REALTIME
/* #endif */
    clock_gettime(CLKID, &tp);
    return (double)tp.tv_sec + 1.0e-9 * (double)tp.tv_nsec;
}

static void
init_stats (void)
{
  needs_init = 0;
}

static void
get_stats (struct stats *s)
{
  s->time = timer ();
}

void
print_stats (FILE *out)
{
  if (needs_init) return;
  fprintf (out, "alg : %s\n", stats_tic_data.statname);
  fprintf (out, "time : %20.17lg\n", (double)(stats_toc_data.time - stats_tic_data.time));
}
#endif

void
stats_tic (char *statname)
{
  if (needs_init)
    init_stats ();
  memset (&stats_tic_data.statname, 0, sizeof (stats_tic_data.statname));
  strncpy ((char*)&stats_tic_data.statname[0], statname, 256);
  get_stats (&stats_tic_data);
}

void
stats_toc (void)
{
  get_stats (&stats_toc_data);
}

void
timer_tic (void)
{
  timer_tic_data = timer ();
}

double
timer_toc (void)
{
  return timer () - timer_tic_data;
}
