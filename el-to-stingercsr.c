#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <string.h>

#include <errno.h>
#include <assert.h>

#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>

#include "compat.h"
#include "xmalloc.h"
#include "xmt-luc.h"
#include "graph-el.h"

static struct el g;
static struct csr gcsr;

static int64_t * mem;

int
main (int argc, char **argv)
{
  const uint64_t endian_check = 0x1234ABCDul;
  int needs_bs;

  if (!argv[1] || !argv[2]) {
    fprintf (stderr, "Usage: %s elbin outfile\n", argv[0]);
    return EXIT_FAILURE;
  }

  needs_bs = snarf_graph (argv[1], &g);
  gcsr = el_to_csr (g);
  //free_graph (g);

  assert (gcsr.xoff);
  assert (gcsr.colind);
  assert (gcsr.weight);

  mem = xmalloc ((1 + 1 + 1 + gcsr.nv+1 + gcsr.ne + gcsr.ne) * sizeof (int64_t));

  fprintf (stderr, "%ld %ld\n", (long)gcsr.nv, (long)gcsr.ne);

  mem[0] = endian_check;
  mem[1] = gcsr.nv;
  mem[2] = gcsr.ne;
  {
    CDECLCSR(gcsr);
    OMP("omp parallel for")
      for (int64_t k = 0; k < gcsr.nv; ++k) {
	const int64_t off = XOFF(gcsr, k);
	const int64_t nextoff = XOFF(gcsr, k+1);
	mem[3+k] = off;
	for (int64_t k2 = off; k2 < nextoff; ++k2) {
	  mem[3+gcsr.nv+1 + k2] = COLJ(gcsr, k2);
	  mem[3+gcsr.nv+1 + gcsr.ne + k2] = COLW(gcsr, k2);
	}
      }
  }
  mem[3+gcsr.nv] = gcsr.ne;

  xmt_luc_snapout (argv[2], mem,
                   (3 + (gcsr.nv + 1) + 2 * gcsr.ne) * sizeof (*mem));

  return EXIT_SUCCESS;
}
