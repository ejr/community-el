/* -*- mode: C; mode: folding; fill-column: 70; -*- */
#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>

#include <unistd.h>

#if defined(_OPENMP)
#include <omp.h>
#endif

#if defined(__MTA__)
#include <machine/runtime.h>
#include <mta_rng.h>
#endif

#include "xmalloc.h"

#if defined(__MTA__)
#define MTA(x) _Pragma(x)
#if defined(MTA_STREAMS)
#define MTASTREAMS() MTA(MTA_STREAMS)
#else
#define MTASTREAMS() MTA("mta use 100 streams")
#endif
#else
#define MTA(x)
#define MTASTREAMS()
#endif

#if defined(_OPENMP)
#define OMP(x) _Pragma(x)
#else
#define OMP(x)
#endif

/* PRNG helpers */
#if !defined(__MTA__)
#if !defined(NRNG)
#if defined(_OPENMP)
#define NRNG 128
#else
#define NRNG 1
#endif
#endif
static int nmtrng;
static unsigned short *eseed;
#else /* __MTA__ */
#if !defined(NRNG)
#define NRNG 100
#endif
static sync int only_one$ = 1;
static int nmtrng;
static RNG * restrict mtrng;
#endif

void
init_urandom (void)
{
    static long seed = 39362;
    const char *seedstr = NULL;

    seedstr = getenv ("SEED");
    if (seedstr)
	seed = strtol (seedstr, NULL, 10);
#if !defined(__MTA__)
    srand48 (seed);
#if defined(_OPENMP)
    nmtrng = omp_get_max_threads ();
#else
    nmtrng = 1;
#endif
    if (nmtrng < NRNG) nmtrng = NRNG;
    eseed = xmalloc (3 * nmtrng * sizeof (*eseed));
    for (size_t k = 0; k < NRNG*3; ++k)
	eseed[k] = (unsigned short)lrand48 ();
OMP("omp barrier")
#else
    nmtrng = MTA_NUM_STREAMS (); /* which can change before we generate. sigh. */
    if (nmtrng < NRNG) nmtrng = NRNG;
    mtrng = init_rn (nmtrng, seed);
#endif
}

MTA("mta expect serial context")
void
uniform_random (size_t nrand, double *rn)
{
    const size_t len = nrand / nmtrng;
    const size_t rem = nrand % nmtrng;
#if !defined(__MTA__)
OMP("omp parallel") {
    OMP("omp for")
    for (int T = 0; T < nmtrng; ++T) {
	const size_t start = (T < rem? T*(len+1) : rem*(len+1) + (T-rem)*len);
	const size_t finish = (T+1 < rem? (T+1)*(len+1) : rem*(len+1) + (T+1-rem)*len);
	for (size_t k = start; k < finish; ++k)
	    do {
	        assert (k < nrand);
		rn[k] = erand48 (&eseed[3*T]);
	    } while (rn[k] == 0.0 || rn[k] == 1.0); /* Exclude endpoints. */
    }
  }
#elif defined(HACKEDUP)
    int x = only_one$;
MTA("mta assert nodep")
MTA("mta assert noalias *rn")
MTASTREAMS()
    for (int T = 0; T < nmtrng; ++T) {
	const size_t start = (T < rem? T*(len+1) : rem*(len+1) + (T-rem)*len);
	const size_t finish = (T+1 < rem? (T+1)*(len+1) : rem*(len+1) + (T+1-rem)*len);
	for (size_t k = start; k < finish; ++k)
	    do {
		rn[k] = get_random_dbl (&mtrng[T]); /* XXX: Docs say in [0,10), experiments say (0,1]. */
	    } while (rn[k] == 0.0 || rn[k] == 1.0); /* Exclude endpoints. */
    }
    only_one$ = x;
#else
    prand (nrand, rn);
#endif
}

MTA("mta expect parallel context")
double
poisson (const double lambda, const double *rn, size_t *rni, const size_t nrand)
/* Knuth's algorithm to generate a Poisson-distributed random number
   by simulation.  This is not the fastest method, but it works
   directly from uniform variates in (0, 1).  Will need O(lambda)
   random numbers.  */
{
    const double L = exp (-lambda);
    size_t i = *rni;
    int k = 0;
    double p = 1.0;
    do {
	if (i >= nrand) return -1; /* Ran out of random numbers. */
	p *= rn[i++];
	++k;
    } while (p > L);
    *rni = i;
    return k - 1;
}

MTA("mta expect parallel context")
double
exponential (const double lambda, const double r)
{
    return - log(r) / lambda;
}

