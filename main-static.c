#define _FILE_OFFSET_BITS 64
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <inttypes.h>

#include <errno.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>

#include "compat.h"
#include "xmalloc.h"
#include "xmt-luc.h"
#include "graph-el.h"
#include "community.h"

extern void stats_tic (char *statname);
extern void stats_toc (void);
extern void timer_tic (void);
extern double timer_toc (void);
extern void print_stats (FILE *out);

extern int use_file_allocation;

static const char *infile = NULL;
static const char *outfile = NULL;
static int outfile_is_text = 0;
static const char *commgraphfile = NULL;
static int64_t max_ncomm = 1;
static int64_t max_commsz = INT64_MAX;

static FILE * info_output_file = NULL;

static struct el g;
#if !defined(NDEBUG)
struct el gcopy;
#endif
static int64_t * c;
static int64_t * csize;
static void * ws;
static int64_t wslen;
static void * lockspace;
static int score_alg = ALG_MB, match_alg = ALG_GREEDY_MAXIMAL;
static int double_self = 0;
static int decimate = 0;
static int use_hist = 0;
static int64_t min_deg = -1, max_deg = -1;
static int64_t nsteps = -1;
static double covlevel = -1;
static double decrease_factor = -1;

int
write_cbin (const char *fname, int64_t *c, const int64_t nv)
{
  if (0 != strcmp (fname, "-"))
    xmt_luc_snapout (fname, c, nv * sizeof (int64_t));
  else
    fwrite (c, sizeof (int64_t), nv, stdout);
  return 0;
}

int
write_ctxt (const char *fname, int64_t *c, const int64_t nv)
{
  FILE * f;
  if (0 != strcmp (fname, "-"))
    f = fopen (fname, "w");
  else
    f = stdout;
  if (!f) return -1;
  for (int64_t k = 0; k < nv; ++k)
    fprintf (f, "%" PRId64 "\n", c[k]);
  fclose (f);
  return 0;
}

static void
usage (FILE *f, const char *name)
{
  fprintf (f,
	   "%s [--outfile=graph-cmap.bin|--textoutfile=graph-cmap.txt]\n"
	   "\t\t[--outgraph=comm-graph-el.bin]\n"
	   "\t\t[--min-degree=#] [--max-degree=#] [--max-num-comm=#] [--max-comm-size=#]\n"
	   "\t\t[--scoring=he|cnm|mb|cond] [--dimacs] [--mb-nstd=#]\n"
	   "\t\t[--matching=greedy|nonmaximal] [--nsteps=#]\n"
	   "\t\t[--cov=#] [--history [--decrease=#]]\n"
	   "\t\tgraph-el.bin\n"
	   "\tDefaults:\n"
	   "\t   scoring = mb, matching = greedy, no history, decrease = 1.1\n",
	   name);
}

static void
parse_args (const int argc, char *argv[])
{
  int arg_err = 0;

  global_verbose = (getenv ("VERBOSE") != NULL);

  while (1) {
    int c;
    static struct option long_options[] = {
      {"outfile", required_argument, 0, 'o'},
      {"textoutfile", required_argument, 0, 't'},
      {"outgraph", required_argument, 0, 'g'},
      {"min-degree", required_argument, 0, 'd'},
      {"max-degree", required_argument, 0, 'D'},
      {"scoring", required_argument, 0, 's'},
      {"matching", required_argument, 0, 'm'},
      {"max-num-comm", required_argument, 0, 'n'},
      {"max-comm-size", required_argument, 0, 'z'},
      {"nsteps", required_argument, 0, 'N'},
      {"mb-nstd", required_argument, 0, 'S'},
      {"cov", optional_argument, 0, 'C'},
      {"decrease", optional_argument, 0, 'T'},
      {"history", no_argument, 0, 'H'},
      {"file-alloc", no_argument, 0, 'f'},
      {"dimacs", no_argument, 0, 'x'},
      {"decimate", no_argument, 0, 'e'},
      {"help", no_argument, 0, 'h'},
      {"verbose", no_argument, 0, 'v'},
      {0, 0, 0, 0}
    };
    int option_index = 0;
    extern char *optarg;
    extern int optind, opterr, optopt;

    c = getopt_long (argc, argv, "s:m:n:z:o:t:g:D:d:N:S:C:T:Hfxeh?v",
		     long_options, &option_index);

    if (-1 == c) break;
    switch (c) {
    case '?':
    case 'h':
      usage (stdout, argv[0]);
      exit (EXIT_SUCCESS);
    case 't':
      outfile_is_text = 1;
    case 'o':
      outfile = optarg;
      break;
    case 'g':
      commgraphfile = optarg;
      break;
    case 'v':
      global_verbose = 1;
      break;
    case 'f':
      use_file_allocation = 1;
      break;
    case 'x':
      double_self = 1;
      break;
    case 'e':
      decimate = 1;
      break;
    case 'H':
      use_hist = 1;
      break;
    case 'C':
      if (optarg) {
	covlevel = strtod (optarg, NULL);
      } else
	covlevel = 0.5;
      if (covlevel > 1) {
	fprintf (stderr, "Improper coverage threshold %s.\n",
		 optarg);
	arg_err = 1;
      }
      break;
    case 'T':
      if (optarg) {
	decrease_factor = strtod (optarg, NULL);
      } else
	decrease_factor = 1.1;
      if (decrease_factor < 1) {
	fprintf (stderr, "Improper decrease factor %s.\n",
		 optarg);
	arg_err = 1;
      }
      break;
    case 'N':
      nsteps = strtol (optarg, NULL, 10);
      if (nsteps < 1) {
	fprintf (stderr, "Improper number of steps %s.\n",
		 optarg);
	arg_err = 1;
      }
      break;
    case 'D':
      max_deg = strtol (optarg, NULL, 10);
      if (max_deg < 1) {
	fprintf (stderr, "Improper maximum degree %s.\n",
		 optarg);
	arg_err = 1;
      }
      break;
    case 'd':
      min_deg = strtol (optarg, NULL, 10);
      if (min_deg < 0) {
	fprintf (stderr, "Improper minimum degree %s.\n",
		 optarg);
	arg_err = 1;
      }
      break;
    case 'S':
      errno = 0;
      mb_nstd = strtod (optarg, NULL);
      if (mb_nstd == 0 && errno) {
	fprintf (stderr, "Improper number of standard deviations %s.\n",
		 optarg);
	arg_err = 1;
      }
      break;
    case 's':
      if (0 == strcasecmp (optarg, "cnm"))
	score_alg = ALG_CNM;
      else if (0 == strcasecmp (optarg, "mb"))
	score_alg = ALG_MB;
      else if (0 == strcasecmp (optarg, "cond") ||
	       0 == strcasecmp (optarg, "conductance"))
	score_alg = ALG_CONDUCTANCE;
      else if (0 == strcasecmp (optarg, "he") ||
	       0 == strcasecmp (optarg, "edge") ||
	       0 == strcasecmp (optarg, "heaviestedge") ||
	       0 == strcasecmp (optarg, "heaviest-edge"))
	score_alg = ALG_HEAVIEST_EDGE;
      else if (0 == strcasecmp (optarg, "rand") ||
	       0 == strcasecmp (optarg, "random") ||
	       0 == strcasecmp (optarg, "rnd"))
	score_alg = ALG_RANDOM;
      else {
	fprintf (stderr, "Unrecognized scoring alg \"%s\".\n", optarg);
	arg_err = 1;
      }
      break;
    case 'm':
      if (0 == strcasecmp (optarg, "nonmaximal"))
	match_alg = ALG_GREEDY_PASS;
      else if (0 == strcasecmp (optarg, "greedy"))
	match_alg = ALG_GREEDY_MAXIMAL;
      else if (0 == strcasecmp (optarg, "tree"))
	match_alg = ALG_TREE;
      else {
	fprintf (stderr, "Unrecognized matching alg \"%s\".\n", optarg);
	arg_err = 1;
      }
      break;
    case 'n':
      max_ncomm = strtol (optarg, NULL, 10);
      if (max_ncomm < 1) {
	fprintf (stderr, "Improper minimum number of communities %s.\n",
		 optarg);
	arg_err = 1;
      }
      break;
    case 'z':
      max_commsz = strtol (optarg, NULL, 10);
      if (max_commsz < 1) {
	fprintf (stderr, "Improper maximum community size %s.\n",
		 optarg);
	arg_err = 1;
      }
      break;
    default:
      arg_err = 1;
      break;
    }
  }

  if (optind < argc)
    infile = argv[optind++];
  else
    arg_err = 1;

  if (outfile && 0 == strcmp(outfile, "-"))
     info_output_file = stderr;
  else
     info_output_file = stdout;

  if (arg_err) {
    usage (stderr, argv[0]);
    exit (EXIT_FAILURE);
  }
}

int
main (int argc, char *argv[])
{
  int64_t nstep;
  double metric;
  double cov, mirrorcov;
  int64_t in_nv, max_in_weight = 0, nonself_in_weight = 0;
  size_t sz;

  parse_args (argc, argv);

  if (global_verbose) { fprintf (info_output_file, "reading %s ...", infile); fflush (stdout); }
  if (!infile || 0 == strcmp (infile, "-"))
       snarf_graph_stdin (&g);
  else
       snarf_graph (infile, &g);
  stats_tic ("dimacs10");
  in_nv = g.nv;
#if !defined(NDEBUG)
  gcopy = copy_graph (g);
#endif
  OMP("omp parallel") {
    CDECL(g);
    int64_t local_max_weight = 0;
    OMP("omp for nowait reduction(+:nonself_in_weight)")
      for (int64_t k = 0; k < g.ne; ++k) {
	assert (W(g, k) >= 1);
	if (W(g, k) > local_max_weight) local_max_weight = W(g, k);
	nonself_in_weight += W(g, k);
      }
    OMP("omp critical") {
      if (local_max_weight > max_in_weight) max_in_weight = local_max_weight;
    }
  }
  if (global_verbose) {
    fprintf (info_output_file, "done: nv %ld, ne %ld\n", (long)g.nv, (long)g.ne);
    fflush (stdout);
  }
  if (global_verbose) { fprintf (info_output_file, "allocating ..."); fflush (stdout); }
  wslen = 3*g.nv + g.ne; /* matching, rowstart, rowend, scores */
  /* off[nv+1] + tailend[2*ne], unmatched[nv]+bestm[nv]+tmp[nv], ws[nv] */
  wslen += 2*g.nv+1 + (2*g.ne > g.nv? 2*g.ne : g.nv);
  sz = wslen * sizeof (int64_t) + g.nv * sizeof (*c) + g.nv * sizeof (*csize);
  if (use_file_allocation)
    c = xmmap_file_alloc (sz);
  else
    c = xmalloc (sz);
  csize = &c[g.nv];
  ws = &csize[g.nv];
#if defined(_OPENMP)
  lockspace = xmalloc (g.nv * sizeof (omp_lock_t));
  {
    omp_lock_t * restrict lk = lockspace;
    OMP("omp parallel for schedule(static)")
      for (intvtx_t i = 0; i < g.nv; ++i) {
	omp_init_lock (&lk[i]);
      }
  }
#else
  lockspace = NULL;
#endif
  if (global_verbose) { fprintf (info_output_file, "done\n"); fflush (stdout); }

  if (global_verbose) { fprintf (info_output_file, "computing ..."); fflush (stdout); }
  nstep = community (c, &g, score_alg, match_alg, double_self, decimate,
		     max_commsz, max_ncomm, min_deg, max_deg, nsteps,
		     covlevel, decrease_factor, use_hist, NULL,
		     csize,
		     ws, wslen, lockspace);
  if (global_verbose) { fprintf (info_output_file, "done: %ld\n", (long)g.nv); fflush (stdout); }

#if !defined(NDEBUG)
  extern int check_adjs (const int64_t * restrict c, int64_t * ws);
  assert (check_adjs (c, ws));
#endif

  stats_toc ();

  if (outfile) {
    if (outfile_is_text)
      write_ctxt (outfile, c, in_nv);
    else
      write_cbin (outfile, c, in_nv);
  }
  if (commgraphfile)
    dump_el (commgraphfile, g, wslen, ws);

  fprintf (info_output_file, "stats::\n");
  fprintf (info_output_file, "nv_orig : %ld\n", (long)in_nv);
  fprintf (info_output_file, "ncomm : %ld\n", (long)g.nv);
  {
    int64_t mxsz = 0;
    OMP("omp parallel for reduction(max: mxsz)")
      for (int64_t k = 0; k < g.nv; ++k)
	if (csize[k] > mxsz) mxsz = csize[k];
    fprintf (info_output_file, "mxsize : %ld\n", (long)mxsz);
  }
  fprintf (info_output_file, "nstep : %ld\n", (long)nstep);
#define PRINT_TIME(x) fprintf (info_output_file, #x " : %20.17g\n", global_##x)
  PRINT_TIME(score_time);
  PRINT_TIME(match_time);
  PRINT_TIME(aftermatch_time);
  PRINT_TIME(contract_time);
  PRINT_TIME(other_time);
  print_stats (info_output_file);

  stats_tic ("conductance");
  metric = eval_conductance_cgraph (g, ws);
  stats_toc ();
  fprintf (info_output_file, "conductance: %20.17g\n", metric);
  print_stats (info_output_file);

  stats_tic ("modularity");
  metric = eval_modularity_cgraph (g, ws);
  stats_toc ();
  fprintf (info_output_file, "modularity: %20.17g\n", metric);
  print_stats (info_output_file);

  stats_tic ("cov");
  eval_cov (g, c, in_nv, max_in_weight, nonself_in_weight,
	    &cov, &mirrorcov, ws);
  stats_toc ();
  fprintf (info_output_file, "cov: %20.17g\n", cov);
  fprintf (info_output_file, "mirrorcov: %20.17g\n", mirrorcov);
  print_stats (info_output_file);

#if defined(_OPENMP)
  {
    omp_lock_t * restrict lk = lockspace;
    OMP("omp parallel for schedule(static)")
      for (intvtx_t i = 0; i < in_nv; ++i)
	omp_destroy_lock (&lk[i]);
  }
  free (lockspace);
#endif
  if (use_file_allocation)
    xmunmap (c, sz);
  else
    free (c);
  g = free_graph (g);

  return EXIT_SUCCESS;
}
