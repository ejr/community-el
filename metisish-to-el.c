/* -*- mode: C; mode: folding; fill-column: 70; -*- */
#define _XOPEN_SOURCE 600
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include <assert.h>

#if !defined(__MTA__)
#include <libgen.h>
#endif
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>

#include "xmalloc.h"
#include "prng.h"

const char *in_name = NULL;
const char *out_name = NULL;

FILE *in;
FILE *out = NULL;

static void
usage (FILE* out, char *progname)
{
  fprintf (out,
	   "%s input.graph output-el.bin\n",
#if !defined(__MTA__)
	   basename (progname)
#else
	   progname
#endif
	   );
}

static void
parse_args (const int argc, char *argv[])
{
  if (argc < 3) {
    usage (stderr, argv[0]);
    exit (EXIT_FAILURE);
  }

  if (0 != strcmp (argv[1], "-")) {
    in_name = argv[1];
    in = fopen (in_name, "r");
    if (!in) {
      fprintf (stderr, "Cannot open input file \"%s\": ", argv[1]);
      perror ("");
      exit (EXIT_FAILURE);
    }
  } else {
    in = stdin;
    in_name = "(STDIN)";
  }

  out_name = argv[2];
  out = fopen (out_name, "w");
  if (!out) {
    fprintf (stderr, "Cannot open output file \"%s\": ", argv[2]);
    perror ("");
    exit (EXIT_FAILURE);
  }
}

static int64_t * base;

static int64_t nent; /* Scatter-gather buffer to handle duplicates within a row. */
static int64_t * restrict loc;
static int64_t * restrict ind;

static int64_t nv = 0, ne_ent = 0, ne = 0, fmt = 0, ncon = 0;

static int64_t * restrict d;

static char rowbuf[16384];

static void
next_line (void)
{
  char * c;
  do {
    memset (rowbuf, 0, sizeof (rowbuf));
    c = fgets (rowbuf, sizeof (rowbuf)-1, in);
  } while (c && *c == '%');
  if (!c) {
    perror ("Confused.");
    abort ();
  }
}

int
main (int argc, char ** argv)
{
  int has_weight = 0;
  size_t alloc_sz;
  const uint64_t endian_check = 0x1234ABCDul;
  parse_args (argc, argv);

  next_line ();
  
  sscanf (rowbuf, "%" SCNd64 " %" SCNd64 " %" SCNd64 " %" SCNd64,
	  &nv, &ne_ent, &fmt, &ncon);
  has_weight = fmt % 10;

  alloc_sz = 5 * nv * sizeof (int64_t);
  base = xmalloc (alloc_sz);

  nent = 0;
  loc = base;
  ind = &loc[nv];
  d = &ind[3*nv];

  fseek (out, (3 + nv) * sizeof (int64_t), SEEK_SET);

  for (int64_t k = 0; k < nv; ++k) loc[k] = -1;
  for (int64_t k = 0; k < nv; ++k) ind[2+2*k] = 0;
  for (int64_t k = 0; k < nv; ++k) d[k] = 0;

  for (int64_t i = 0; i < nv; ++i) {
    int64_t k, j, w = 1;
    size_t line_len;
    int pos = 0, cp, posold;
    next_line ();
    line_len = strlen (rowbuf);
    /* fprintf (stderr, "%" PRId64  "...:", i); */
    for (int k2 = 0; k2 < ncon; ++k2) { /* Read and ignore. */
      sscanf (&rowbuf[pos], "%" SCNd64 "%n", &k, &cp);
      pos += cp;
    }
    do {
      posold = pos;
      j = -1;
      sscanf (&rowbuf[pos], "%" SCNd64 "%n", &j, &cp);
      if (j <= 0) break;
      --j;
      pos += cp;
      if (has_weight) {
	sscanf (&rowbuf[pos], "%" SCNd64 "%n", &w, &cp);
	pos += cp;
      }
      /* fprintf (stderr, " (%" PRId64 "; %" PRId64 ")", j+1, w); */
      if (i == j) {
	d[i] += w;
      } else if (i > j) {
	if (loc[j] < 0) {
	  loc[j] = nent++;
	  ind[0+3*loc[j]] = i;
	  ind[1+3*loc[j]] = j;
	}
	ind[2+3*loc[j]] += w;
      }
      /* Assume symmetric and ignore i < j. */

    } while (pos < line_len && pos != posold);

    fwrite (ind, sizeof (*ind), 3*nent, out);
    /*
    for (int64_t k = 0; k < nent; ++k) {
      fprintf (stderr, "   %" PRId64 " %" PRId64 " %" PRId64 "\n",
	       ind[0+3*k], ind[1+3*k], ind[2+3*k]);
    }
    */

    for (int64_t k = 0; k < nent; ++k) {
      loc[ind[1+3*k]] = -1;
      ind[2+3*k] = 0;
    }
    ne += nent;
    nent = 0;

    /* fprintf (stderr, "\n"); */
  }

  fseek (out, 0, SEEK_SET);
  fwrite (&endian_check, sizeof (endian_check), 1, out);
  fwrite (&nv, sizeof (nv), 1, out);
  fwrite (&ne, sizeof (ne), 1, out);
  fwrite (d, sizeof (*d), nv, out);

  fclose (out);

  free (base);
  return EXIT_SUCCESS;
}
