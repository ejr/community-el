/* -*- mode: C; fill-column: 70; -*- */
/* For sanity... */
#define _XOPEN_SOURCE 600
/* For MAP_ANON in some places... */
#define _SVID_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <errno.h>

#include <unistd.h>
#include <sys/mman.h>

#include "xmalloc.h"

void *
xmalloc (size_t sz)
{
    void * out;
    out = malloc (sz);
    if (!out) {
	perror ("Failed xmalloc: ");
	abort ();
    }
    return out;
}

void *
xcalloc (size_t nelem, size_t sz)
{
    void * out;
    out = calloc (nelem, sz);
    if (!out) {
	perror ("Failed xcalloc: ");
	abort ();
    }
    return out;
}

void *
xrealloc (void *p, size_t sz)
{
    void * out;
    out = realloc (p, sz);
    if (!out) {
	perror ("Failed xrealloc: ");
	abort ();
    }
    return out;
}

void *
xmmap (void *addr, size_t len, int prot, int flags, int fd, off_t offset)
{
  void * out;
  out = mmap (addr, len, prot, flags, fd, offset);
  if (MAP_FAILED != out) return out;
  perror ("mmap failed");
  abort ();
  return NULL;
}

void *
xmmap_alloc (size_t sz)
{
  return xmmap (NULL, sz, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
}

void *
xmmap_file_alloc (size_t sz)
{
  const char *tmpdir = "/tmp";
  size_t tmpdirlen;
  char *tmpl;
  int fd;
  void *out;
  if (getenv ("TMPDIR")) {
    tmpdir = getenv ("TMPDIR");
  } else if (getenv ("TEMPDIR")) {
    tmpdir = getenv ("TEMPDIR");
  }
  if ((tmpdirlen = strlen (tmpdir)) >= PATH_MAX) {
    fprintf (stderr, "tmpdir far too long\n");
    abort ();
  }
  tmpl = xmalloc (tmpdirlen + 8);
  strcpy (tmpl, tmpdir);
  if (tmpl[tmpdirlen - 1] != '/') {
    tmpl[tmpdirlen++] = '/';
    tmpl[tmpdirlen] = 0;
  }
  for (size_t k = tmpdirlen; k < tmpdirlen + 6; ++k)
    tmpl[k] = 'X';
  tmpl[tmpdirlen + 6] = 0;
  fd = mkstemp (tmpl);
  unlink (tmpl);
  out = xmmap (NULL, sz, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, fd, 0);
  close (fd);
  return out;
}

void
xmunmap (void * p, size_t sz)
{
  munmap (p, sz);
}
