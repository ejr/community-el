#define _FILE_OFFSET_BITS 64
#define _XOPEN_SOURCE 700
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <inttypes.h>

#include <errno.h>
#include <assert.h>

#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>

#if defined(USE_ALLOCA)
#include <alloca.h>
#endif

#include "compat.h"
#include "xmalloc.h"
#include "xmt-luc.h"
#include "graph-el.h"

int use_file_allocation = 0;

static int io_ready = 0;

static void
io_init (void)
{
#if defined(__MTA__)
    if (!io_ready) {
	extern void xmt_luc_io_init (void);
	xmt_luc_io_init ();
    }
#endif
    io_ready = 1;
}

MTA("mta inline")
MTA("mta expect parallel context")
static int64_t bs64 (int64_t)
#if defined(__GNUC__)
    __attribute__((const))
#endif
    ;

static int64_t
bs64 (int64_t xin)
{
    uint64_t x = (uint64_t)xin; /* avoid sign-extension issues */
    x = (x >> 32) | (x << 32);
    x = ((x & ((uint64_t)0xFFFF0000FFFF0000ull)) >> 16)
	| ((x & ((uint64_t)0x0000FFFF0000FFFFull)) << 16);
    x = ((x & ((uint64_t)0xFF00FF00FF00FF00ull)) >> 8)
	| ((x & ((uint64_t)0x00FF00FF00FF00FFull)) << 8);
    return x;
}

static void
bs64_n (size_t n, int64_t * restrict d)
{
  OMP("omp parallel for")
  MTA("mta assert nodep")
  for (size_t k = 0; k < n; ++k)
    d[k] = bs64 (d[k]);
}

static ssize_t
xread(int fd, void *databuf, size_t len)
{
    unsigned char *buf = (unsigned char*)databuf;
    size_t totread = 0, toread = len;
    ssize_t lenread;
    while (totread < len) {
      lenread = read (fd, &buf[totread], toread);
      if ((lenread < 0) && (errno == EAGAIN || errno == EINTR))
	continue;
      else if (lenread < 0) break;
      toread -= lenread;
      totread += lenread;
    }
    return totread;
}

struct el
alloc_graph (size_t sz)
{
  struct el out;
  intvtx_t * mem;
  memset (&out, 0, sizeof (out));
  if (use_file_allocation)
    mem = xmmap_file_alloc (sz);
  else
    mem = xmmap_alloc (sz);
  out.mem = mem;
  out.memsz = sz;
  out.d = &mem[3];
  return out;
}

struct el
setup_alloced_graph (struct el out, int64_t nv, int64_t ne)
{
  out.nv = out.nv_orig = nv;
  out.ne = out.ne_orig = ne;
  out.mem[1] = nv;
  out.mem[2] = ne;
  out.el = &out.d[nv];
  return out;
}

struct el
copy_graph (struct el in)
{
  struct el out;

  memset (&out, 0, sizeof (out));
  out = alloc_graph (in.memsz);
  out = setup_alloced_graph (out, in.nv, in.ne);
  memcpy (out.mem, in.mem, in.memsz);
  return out;
}

struct el
free_graph (struct el out)
{
  if (out.mem)
    xmunmap (out.mem, out.memsz);
  memset (&out, 0, sizeof (out));
  return out;
}

int
snarf_graph (const char * fname,
	     struct el * g)
{
    const uint64_t endian_check = 0x1234ABCDul;
    size_t sz;
    ssize_t ssz;
    int64_t *mem;
    int needs_bs = 0;

    io_init ();
    xmt_luc_stat (fname, &sz);

    if (sz % sizeof (*mem)) {
	fprintf (stderr, "graph file size is not a multiple of sizeof (int64_t)\n");
	abort ();
    }

#if defined(USE32BIT)
    *g = alloc_graph (sz/2);
    mem = xmalloc (sz);
#else
    *g = alloc_graph (sz);
    mem = g->mem;
#endif

#if !defined(__MTA__)
    {
	int fd;
	if ( (fd = open (fname, O_RDONLY)) < 0) {
	    perror ("Error opening initial graph");
	    abort ();
	}
	ssz = xread (fd, mem, sz);
	if (sz != ssz) {
	    fprintf (stderr, "XXX: %zu %zd %zd\n", sz, ssz, SSIZE_MAX);
	    perror ("Error reading initial graph");
	    abort ();
	}
	/*xread (fd, mem, sz);*/
	close (fd);
    }
#else /* __MTA__ */
    {
	extern void xmt_luc_snapin (const char*, void*, size_t);
	xmt_luc_snapin (fname, mem, sz);
    }
#endif /* __MTA__ */

    if (endian_check != *mem) {
      needs_bs = 1;
      bs64_n (sz / sizeof (*mem), mem);
    }

#if defined(USE32BIT)
    OMP("omp parallel for")
    for (size_t k = 0; k < sz / sizeof (*mem); ++k)
      g->mem[k] = mem[k];
#endif

    *g = setup_alloced_graph (*g, mem[1], mem[2]);

#if defined(USE32BIT)
    free (mem);
#endif
    return needs_bs;
}

int
snarf_graph_stdin (struct el * g)
{
    const uint64_t endian_check = 0x1234ABCDul;
    size_t sz;
    ssize_t ssz;
    int64_t hdr[3];
    int64_t nv, ne;
    int needs_bs = 0;

    io_init ();

    ssz = xread (STDIN_FILENO, hdr, sizeof (hdr));
    if (ssz != sizeof (hdr)) {
         perror ("Error reading initial graph header from stdin");
         abort ();
    }

    needs_bs = endian_check != hdr[0];

    if (needs_bs) {
         nv = bs64 (hdr[1]);
         ne = bs64 (hdr[2]);
    } else {
         nv = hdr[1];
         ne = hdr[2];
    }

    sz = (3 + nv + 3*ne) * sizeof (int64_t);

#if defined(USE32BIT)
    *g = alloc_graph (sz/2);
#else
    *g = alloc_graph (sz);
#endif

    g->mem[0] = endian_check;
    g->mem[1] = nv;
    g->mem[2] = ne;
    for (size_t k = 0; k < nv; ++k) {
         int64_t tmp;
         if (sizeof (tmp) != xread (STDIN_FILENO, &tmp, sizeof (tmp)))
              goto err_reading;
         g->mem[3+k] = (needs_bs? bs64 (tmp) : tmp);
    }
    for (size_t k = 0; k < ne; ++k) {
         int64_t line[3];
         if (sizeof (line) != xread (STDIN_FILENO, line, sizeof (line)))
              goto err_reading;
         g->mem[3+nv+3*k] = (needs_bs? bs64 (line[0]) : line[0]);
         g->mem[3+nv+3*k+1] = (needs_bs? bs64 (line[1]) : line[1]);
         g->mem[3+nv+3*k+2] = (needs_bs? bs64 (line[2]) : line[2]);
    }

    *g = setup_alloced_graph (*g, nv, ne);
    return needs_bs;

err_reading:
    perror ("Error reading initial graph data from stdin");
    abort ();
}

static ssize_t
xwrite(int fd, const void *buf, size_t len)
{
    ssize_t lenwritten;
    while (1) {
	lenwritten = write(fd, buf, len);
	if ((lenwritten < 0) && (errno == EAGAIN || errno == EINTR))
	    continue;
	return lenwritten;
    }
}

void
dump_el (const char * fname,
	 const struct el g,
	 const size_t niwork, int64_t * restrict iwork)
{
    const uint64_t endian_check = 0x1234ABCDul;
    int64_t nv = g.nv;
    int64_t ne = g.ne;
    CDECL(g);

#if !defined(__MTA__)
    /* The sane option. */
    int fd;
    if ( (fd = open (fname, O_WRONLY|O_CREAT|O_TRUNC,
		     S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)) < 0) {
      perror ("Error opening output graph");
      abort ();
    }
    if (sizeof (endian_check) != xwrite (fd, &endian_check,
					 sizeof (endian_check)))
	goto err;

    if (sizeof (nv) != xwrite (fd, &nv, sizeof (nv)))
	goto err;

    if (sizeof (ne) != xwrite (fd, &ne, sizeof (ne)))
	goto err;

#if defined(USE32BIT)
    OMP("omp parallel") {
      OMP("omp for nowait")
	for (size_t k = 0; k < nv; ++k)
	  iwork[k] = D(g, k);
      OMP("omp for")
	for (size_t k = 0; k < ne; ++k) {
	  iwork[nv+3*k] = I(g, k);
	  iwork[nv+3*k+1] = J(g, k);
	  iwork[nv+3*k+2] = W(g, k);
	}
    }

    if ((nv + 3 * ne) * sizeof (*iwork) != xwrite (fd, iwork,
						   (nv + 3*ne) * sizeof (*iwork)))
	goto err;
#else
    if (nv * sizeof (D(g, 0)) != xwrite (fd, &D(g, 0), nv * sizeof (D(g, 0))))
	goto err;

    if (3 * ne * sizeof (I(g, 0)) != xwrite (fd, &I(g, 0), 3 * ne * sizeof (I(g, 0))))
	goto err;
#endif

#else /* __MTA__ inanity */
    extern void xmt_luc_snapout (const char*, void*, size_t);
    assert (niwork >= 3*(ne + 1));
    iwork[0] = endian_check;
    iwork[1] = nv;
    iwork[2] = ne;
    MTA("mta assert nodep")
    for (size_t k = 0; k < nv; ++k)
      iwork[3 + k] = D(g, k);
    MTA("mta assert nodep")
    for (size_t k = 0; k < ne; ++k) {
      iwork[3 + nv + 3*k] = I(g, k);
      iwork[3 + nv + 3*k+1] = J(g, k);
      iwork[3 + nv + 3*k+2] = W(g, k);
    }
    xmt_luc_snapout (fname, iwork, (3 + nv + 3*ne) * sizeof (*iwork));
#endif

    return;
  err:
    perror ("Error writing initial community graph: ");
    abort ();    
}

struct csr
alloc_csr (intvtx_t nv, intvtx_t ne)
{
  struct csr out;
  size_t sz;
  intvtx_t * mem;
  memset (&out, 0, sizeof (out));
  sz = (nv+1 + 2*ne) * sizeof (intvtx_t);
  if (use_file_allocation)
    mem = xmmap_file_alloc (sz);
  else
    mem = xmmap_alloc (sz);
  out.nv = nv;
  out.ne = ne;
  out.mem = mem;
  out.memsz = sz;
  out.xoff = mem;
  out.colind = &out.xoff[nv+1];
  out.weight = &out.colind[ne];
  return out;
}

struct csr
free_csr (struct csr out)
{
  if (out.mem)
    xmunmap (out.mem, out.memsz);
  memset (&out, 0, sizeof (out));
  return out;
}

#if defined(_OPENMP)
static intvtx_t *buf;

static intvtx_t
prefix_sum (const intvtx_t n, intvtx_t *ary)
{
  int nt, tid;
  intvtx_t slice_begin, slice_end, t1, t2, k, tmp;

  nt = omp_get_num_threads ();
  tid = omp_get_thread_num ();
#if defined(USE_ALLOCA)
  OMP("omp master")
    buf = alloca (nt * sizeof (*buf));
#else
  OMP("omp master")
    buf = xmalloc (nt * sizeof (*buf));
#endif
  OMP("omp flush (buf)");
  OMP("omp barrier");

  t1 = n / nt;
  t2 = n % nt;
  slice_begin = t1 * tid + (tid < t2? tid : t2);
  slice_end = t1 * (tid+1) + ((tid+1) < t2? (tid+1) : t2);

  tmp = 0;
  for (k = slice_begin; k < slice_end; ++k)
    tmp += ary[k];
  buf[tid] = tmp;
  OMP("omp barrier");
  OMP("omp single")
    for (k = 1; k < nt; ++k)
      buf[k] += buf[k-1];
  OMP("omp barrier");
  tmp = buf[nt-1];
  if (tid)
    t1 = buf[tid-1];
  else
    t1 = 0;
  for (k = slice_begin; k < slice_end; ++k) {
    intvtx_t t = ary[k];
    ary[k] = t1;
    t1 += t;
  }
#if defined(USE_ALLOCA)
  OMP("omp barrier");
  OMP("omp master")
    free (buf);
#endif
  OMP("omp barrier");
  return t1;
}
#else
MTA("mta inline")
static intvtx_t
prefix_sum (const intvtx_t n, intvtx_t *ary)
{
  intvtx_t cur = 0;
  for (intvtx_t k = 0; k < n; ++k) {
    intvtx_t tmp = ary[k];
    ary[k] = cur;
    cur += tmp;
  }
  return cur;
}
#endif

#if defined(USE32BIT)
#define IFA(p,i) int32_fetch_add ((p), (i)) 
#else
#define IFA(p,i) int64_fetch_add ((p), (i)) 
#endif

struct csr
el_to_csr (const struct el gel)
{
  const intvtx_t nv = gel.nv;
  const intvtx_t ne_in = gel.ne;
  intvtx_t ne_out = 2*ne_in;
  CDECL(gel);
  struct csr gcsr;

  OMP("omp parallel") {
    OMP("omp for reduction(+:ne_out)")
      for (intvtx_t i = 0; i < nv; ++i)
	if (D(gel, i)) ++ne_out;

    OMP("omp single") {
      gcsr = alloc_csr (nv, ne_out);
    }
    OMP("omp flush (gcsr)");
    OMP("omp barrier");

    DECLCSR(gcsr);

    OMP("omp for")
      for (intvtx_t i = 0; i < nv; ++i)
	if (D(gel, i)) XOFF(gcsr, i+1) = 1;

    OMP("omp for")
      for (intvtx_t k = 0; k < ne_in; ++k) {
	const intvtx_t i = I(gel, k);
	const intvtx_t j = J(gel, k);
	IFA (&XOFF(gcsr, i+1), 1);
	IFA (&XOFF(gcsr, j+1), 1);
      }

    prefix_sum (nv, &XOFF(gcsr, 1));

    OMP("omp for")
      for (intvtx_t i = 0; i < nv; ++i)
	if (D(gel, i)) {
	  const size_t loc = XOFF(gcsr, i+1);
	  COLJ(gcsr, loc) = i;
	  COLW(gcsr, loc) = D(gel, i);
	  ++XOFF(gcsr, i+1);
	}

    OMP("omp for")
      for (intvtx_t k = 0; k < ne_in; ++k) {
	const intvtx_t i = I(gel, k);
	const intvtx_t j = J(gel, k);
	const intvtx_t w = W(gel, k);
	const intvtx_t iloc = IFA (&XOFF(gcsr, i+1), 1);
	const intvtx_t jloc = IFA (&XOFF(gcsr, j+1), 1);
	COLJ(gcsr, iloc) = j;
	COLW(gcsr, iloc) = w;
	COLJ(gcsr, jloc) = i;
	COLW(gcsr, jloc) = w;
      }
    assert (XOFF(gcsr, nv) == ne_out);
  }

  return gcsr;
}
